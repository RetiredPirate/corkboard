import { Field, Int, InputType } from 'type-graphql'

@InputType()
export class NewNote {
    @Field()
    corkboardId: string

    @Field({ nullable: true })
    title: string

    @Field({ nullable: true })
    body: string

    @Field(() => Int, { nullable: true })
    xPos: number

    @Field(() => Int, { nullable: true })
    yPos: number
}
