import { Resolver, Query, Mutation, Arg } from 'type-graphql'
import { Note } from './Note'
import { NewNote } from './NewNote'
import { UpdateNote } from './UpdateNote'

@Resolver()
export class NoteResolver {
    @Query(() => [Note])
    notes(): Promise<Array<Note>> {
        return Note.find()
    }

    @Mutation(() => Note)
    async createNote(
        @Arg('newNote', { nullable: true }) newNote: NewNote
    ): Promise<Note> {
        const currentLength = (await Note.find()).length

        const createdNote: Partial<Note> = {
            body: '',
            title: '',
            xPos: 500,
            yPos: 500,
            sequence: currentLength,
            createdDateTime: new Date(),
            updatedDateTime: new Date(),
            ...newNote
        }

        const createResult = await Note.insert(createdNote)

        const returnNote: Note = <Note>{
            id: createResult.generatedMaps[0].id,
            ...createdNote
        }
        return returnNote
    }

    @Mutation(() => Note)
    async updateNote(
        @Arg('updateNote') { id, ...updateNote }: UpdateNote
    ): Promise<Note> {
        const allNotes = await Note.find()

        const existingNote = allNotes.find(n => n.id.toString() == id)
        if (!existingNote) {
            throw new Error('Note note found')
        }

        const restOfNotes = allNotes.filter(n => n.id.toString() != id)

        const sortedList = restOfNotes.sort((n1, n2) =>
            n1.sequence < n2.sequence ? -1 : 1
        )

        const noteToUpdate: Note = <Note>{
            ...existingNote,
            ...updateNote,
            updatedDateTime: new Date()
        }

        pushNoteToTop(sortedList, noteToUpdate)
        await Note.save(sortedList)

        return Note.findOne(id) as Promise<Note>
    }

    @Mutation(() => Boolean)
    async deleteNote(@Arg('noteId') noteId: string): Promise<boolean> {
        await Note.delete(noteId)
        const allNotes = await Note.find()

        for (let i = 0; i < allNotes.length; i++) {
            allNotes[i].sequence = i
        }

        await Note.save(allNotes)

        return true
    }
}

function pushNoteToTop(notes: Note[], noteToPush: Note): void {
    notes.push(noteToPush)

    for (let i = 0; i < notes.length; i++) {
        notes[i].sequence = i
    }
}
