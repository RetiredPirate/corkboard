import {
    Entity,
    Column,
    BaseEntity,
    ObjectID,
    ObjectIdColumn,
    ManyToOne
} from 'typeorm'
import { ObjectType, Field, Int } from 'type-graphql'
import { Corkboard } from '../Corkboard/Corkboard'

@ObjectType()
@Entity('notes')
export class Note extends BaseEntity {
    @Field(() => String)
    @ObjectIdColumn()
    id: ObjectID

    @Field()
    @Column()
    corkboardId: string

    @Field()
    @Column()
    title: string

    @Field()
    @Column()
    body: string

    @Field(() => Int)
    @Column()
    xPos: number

    @Field(() => Int)
    @Column()
    yPos: number

    @Field(() => Int)
    @Column()
    sequence: number

    @Field()
    @Column()
    createdDateTime: Date

    @Field()
    @Column()
    updatedDateTime: Date

    @ManyToOne(
        type => Corkboard,
        corkboard => corkboard.notes
    )
    corkboard: Corkboard
}
