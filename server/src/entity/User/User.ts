import { Entity, Column, BaseEntity, ObjectID, ObjectIdColumn } from 'typeorm'
import { ObjectType, Field } from 'type-graphql'

@ObjectType()
@Entity('users')
export class User extends BaseEntity {
    @Field(() => String)
    @ObjectIdColumn()
    id: ObjectID

    @Field()
    @Column()
    email: string

    @Column()
    password: string

    @Column('int', { default: 0 })
    tokenVersion: number
}
