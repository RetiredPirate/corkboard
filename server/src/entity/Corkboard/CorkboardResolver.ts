import { Resolver, Query, Mutation, FieldResolver, Root } from 'type-graphql'
import { Corkboard } from './Corkboard'
import { Note } from '../Note/Note'
import { AdvancedConsoleLogger } from 'typeorm'

@Resolver(of => Corkboard)
export class CorkboardResolver {
    @Query(() => [Corkboard])
    corkboards(): Promise<Array<Corkboard>> {
        let data = Corkboard.find()
        data.then(actualData => console.log(actualData))

        return data
    }

    @Mutation(() => Corkboard)
    async createCorkboard(): Promise<Corkboard> {
        const corkboardToCreate: Partial<Corkboard> = {
            createdDateTime: new Date(),
            updatedDateTime: new Date(),
            notes: []
        }

        const createResult = await Corkboard.insert(corkboardToCreate)

        const returnCorkboard: Corkboard = <Corkboard>{
            id: createResult.generatedMaps[0].id,
            ...corkboardToCreate
        }
        return returnCorkboard
    }

    @FieldResolver()
    notes(@Root() corkboard: Corkboard): Promise<Array<Note>> {
        return Note.find({
            corkboardId: corkboard.id.toString()
        })
    }
}
