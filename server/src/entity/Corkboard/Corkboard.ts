import {
    Entity,
    Column,
    BaseEntity,
    ObjectID,
    ObjectIdColumn,
    OneToMany
} from 'typeorm'
import { ObjectType, Field } from 'type-graphql'
import { Note } from '../Note/Note'

@ObjectType()
@Entity('corkboards')
export class Corkboard extends BaseEntity {
    @Field(() => String)
    @ObjectIdColumn()
    id: ObjectID

    @Field()
    @Column()
    createdDateTime: Date

    @Field()
    @Column()
    updatedDateTime: Date

    @Field(() => Note)
    @OneToMany(
        () => Note,
        note => note.corkboard
    )
    notes: Note[]
}
