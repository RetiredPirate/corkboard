import React from 'react'
import ThemeContext, { IThemeModel } from './ThemeContext/ThemeContext'
import useTheme from './ThemeContext/useTheme'
import Root from './Root/Root'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'

const App: React.FC = () => {
  const themeHook: [IThemeModel, Function] = useTheme()

  const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql'
  })

  return (
    <ApolloProvider client={client}>
      <ThemeContext.Provider value={themeHook}>
        <Root></Root>
      </ThemeContext.Provider>
    </ApolloProvider>
  )
}

export default App
