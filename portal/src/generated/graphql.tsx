import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  DateTime: any,
};

export type Corkboard = {
   __typename?: 'Corkboard',
  id: Scalars['String'],
  createdDateTime: Scalars['DateTime'],
  updatedDateTime: Scalars['DateTime'],
  notes: Array<Note>,
};


export type LoginResponse = {
   __typename?: 'LoginResponse',
  accessToken: Scalars['String'],
};

export type Mutation = {
   __typename?: 'Mutation',
  revokeRefreshTokensForUser: Scalars['Boolean'],
  login: LoginResponse,
  register: Scalars['Boolean'],
  createNote: Note,
  updateNote: Note,
  deleteNote: Scalars['Boolean'],
  createCorkboard: Corkboard,
};


export type MutationRevokeRefreshTokensForUserArgs = {
  userId: Scalars['Int']
};


export type MutationLoginArgs = {
  password: Scalars['String'],
  email: Scalars['String']
};


export type MutationRegisterArgs = {
  password: Scalars['String'],
  email: Scalars['String']
};


export type MutationCreateNoteArgs = {
  newNote?: Maybe<NewNote>
};


export type MutationUpdateNoteArgs = {
  updateNote: UpdateNote
};


export type MutationDeleteNoteArgs = {
  noteId: Scalars['String']
};

export type NewNote = {
  corkboardId: Scalars['String'],
  title?: Maybe<Scalars['String']>,
  body?: Maybe<Scalars['String']>,
  xPos?: Maybe<Scalars['Int']>,
  yPos?: Maybe<Scalars['Int']>,
};

export type Note = {
   __typename?: 'Note',
  id: Scalars['String'],
  corkboardId: Scalars['String'],
  title: Scalars['String'],
  body: Scalars['String'],
  xPos: Scalars['Int'],
  yPos: Scalars['Int'],
  sequence: Scalars['Int'],
  createdDateTime: Scalars['DateTime'],
  updatedDateTime: Scalars['DateTime'],
};

export type Query = {
   __typename?: 'Query',
  hello: Scalars['String'],
  bye: Scalars['String'],
  users: Array<User>,
  notes: Array<Note>,
  corkboards: Array<Corkboard>,
};

export type UpdateNote = {
  id: Scalars['String'],
  title?: Maybe<Scalars['String']>,
  body?: Maybe<Scalars['String']>,
  xPos?: Maybe<Scalars['Int']>,
  yPos?: Maybe<Scalars['Int']>,
};

export type User = {
   __typename?: 'User',
  id: Scalars['String'],
  email: Scalars['String'],
};

export type CorkboardQueryVariables = {};


export type CorkboardQuery = (
  { __typename?: 'Query' }
  & { corkboards: Array<(
    { __typename?: 'Corkboard' }
    & Pick<Corkboard, 'id'>
    & { notes: Array<(
      { __typename?: 'Note' }
      & Pick<Note, 'title' | 'body' | 'xPos' | 'yPos' | 'sequence'>
    )> }
  )> }
);

export type CreateCorkboardMutationVariables = {};


export type CreateCorkboardMutation = (
  { __typename?: 'Mutation' }
  & { createCorkboard: (
    { __typename?: 'Corkboard' }
    & Pick<Corkboard, 'id'>
  ) }
);

export type NotesQueryVariables = {};


export type NotesQuery = (
  { __typename?: 'Query' }
  & { notes: Array<(
    { __typename?: 'Note' }
    & Pick<Note, 'id' | 'title' | 'body' | 'xPos' | 'yPos' | 'sequence'>
  )> }
);

export type UpdateNoteMutationVariables = {
  input: UpdateNote
};


export type UpdateNoteMutation = (
  { __typename?: 'Mutation' }
  & { updateNote: (
    { __typename?: 'Note' }
    & Pick<Note, 'id'>
  ) }
);

export type CreateNoteMutationVariables = {
  input: NewNote
};


export type CreateNoteMutation = (
  { __typename?: 'Mutation' }
  & { createNote: (
    { __typename?: 'Note' }
    & Pick<Note, 'id' | 'xPos' | 'yPos' | 'sequence' | 'corkboardId'>
  ) }
);

export type DeleteNoteMutationVariables = {
  id: Scalars['String']
};


export type DeleteNoteMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteNote'>
);


export const CorkboardDocument = gql`
    query Corkboard {
  corkboards {
    id
    notes {
      title
      body
      xPos
      yPos
      sequence
    }
  }
}
    `;

/**
 * __useCorkboardQuery__
 *
 * To run a query within a React component, call `useCorkboardQuery` and pass it any options that fit your needs.
 * When your component renders, `useCorkboardQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCorkboardQuery({
 *   variables: {
 *   },
 * });
 */
export function useCorkboardQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<CorkboardQuery, CorkboardQueryVariables>) {
        return ApolloReactHooks.useQuery<CorkboardQuery, CorkboardQueryVariables>(CorkboardDocument, baseOptions);
      }
export function useCorkboardLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<CorkboardQuery, CorkboardQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<CorkboardQuery, CorkboardQueryVariables>(CorkboardDocument, baseOptions);
        }
export type CorkboardQueryHookResult = ReturnType<typeof useCorkboardQuery>;
export type CorkboardLazyQueryHookResult = ReturnType<typeof useCorkboardLazyQuery>;
export type CorkboardQueryResult = ApolloReactCommon.QueryResult<CorkboardQuery, CorkboardQueryVariables>;
export const CreateCorkboardDocument = gql`
    mutation CreateCorkboard {
  createCorkboard {
    id
  }
}
    `;
export type CreateCorkboardMutationFn = ApolloReactCommon.MutationFunction<CreateCorkboardMutation, CreateCorkboardMutationVariables>;

/**
 * __useCreateCorkboardMutation__
 *
 * To run a mutation, you first call `useCreateCorkboardMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCorkboardMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCorkboardMutation, { data, loading, error }] = useCreateCorkboardMutation({
 *   variables: {
 *   },
 * });
 */
export function useCreateCorkboardMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateCorkboardMutation, CreateCorkboardMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateCorkboardMutation, CreateCorkboardMutationVariables>(CreateCorkboardDocument, baseOptions);
      }
export type CreateCorkboardMutationHookResult = ReturnType<typeof useCreateCorkboardMutation>;
export type CreateCorkboardMutationResult = ApolloReactCommon.MutationResult<CreateCorkboardMutation>;
export type CreateCorkboardMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateCorkboardMutation, CreateCorkboardMutationVariables>;
export const NotesDocument = gql`
    query Notes {
  notes {
    id
    title
    body
    xPos
    yPos
    sequence
  }
}
    `;

/**
 * __useNotesQuery__
 *
 * To run a query within a React component, call `useNotesQuery` and pass it any options that fit your needs.
 * When your component renders, `useNotesQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useNotesQuery({
 *   variables: {
 *   },
 * });
 */
export function useNotesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<NotesQuery, NotesQueryVariables>) {
        return ApolloReactHooks.useQuery<NotesQuery, NotesQueryVariables>(NotesDocument, baseOptions);
      }
export function useNotesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<NotesQuery, NotesQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<NotesQuery, NotesQueryVariables>(NotesDocument, baseOptions);
        }
export type NotesQueryHookResult = ReturnType<typeof useNotesQuery>;
export type NotesLazyQueryHookResult = ReturnType<typeof useNotesLazyQuery>;
export type NotesQueryResult = ApolloReactCommon.QueryResult<NotesQuery, NotesQueryVariables>;
export const UpdateNoteDocument = gql`
    mutation UpdateNote($input: UpdateNote!) {
  updateNote(updateNote: $input) {
    id
  }
}
    `;
export type UpdateNoteMutationFn = ApolloReactCommon.MutationFunction<UpdateNoteMutation, UpdateNoteMutationVariables>;

/**
 * __useUpdateNoteMutation__
 *
 * To run a mutation, you first call `useUpdateNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateNoteMutation, { data, loading, error }] = useUpdateNoteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateNoteMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateNoteMutation, UpdateNoteMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateNoteMutation, UpdateNoteMutationVariables>(UpdateNoteDocument, baseOptions);
      }
export type UpdateNoteMutationHookResult = ReturnType<typeof useUpdateNoteMutation>;
export type UpdateNoteMutationResult = ApolloReactCommon.MutationResult<UpdateNoteMutation>;
export type UpdateNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateNoteMutation, UpdateNoteMutationVariables>;
export const CreateNoteDocument = gql`
    mutation CreateNote($input: NewNote!) {
  createNote(newNote: $input) {
    id
    xPos
    yPos
    sequence
    corkboardId
  }
}
    `;
export type CreateNoteMutationFn = ApolloReactCommon.MutationFunction<CreateNoteMutation, CreateNoteMutationVariables>;

/**
 * __useCreateNoteMutation__
 *
 * To run a mutation, you first call `useCreateNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createNoteMutation, { data, loading, error }] = useCreateNoteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateNoteMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateNoteMutation, CreateNoteMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateNoteMutation, CreateNoteMutationVariables>(CreateNoteDocument, baseOptions);
      }
export type CreateNoteMutationHookResult = ReturnType<typeof useCreateNoteMutation>;
export type CreateNoteMutationResult = ApolloReactCommon.MutationResult<CreateNoteMutation>;
export type CreateNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateNoteMutation, CreateNoteMutationVariables>;
export const DeleteNoteDocument = gql`
    mutation DeleteNote($id: String!) {
  deleteNote(noteId: $id)
}
    `;
export type DeleteNoteMutationFn = ApolloReactCommon.MutationFunction<DeleteNoteMutation, DeleteNoteMutationVariables>;

/**
 * __useDeleteNoteMutation__
 *
 * To run a mutation, you first call `useDeleteNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteNoteMutation, { data, loading, error }] = useDeleteNoteMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteNoteMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteNoteMutation, DeleteNoteMutationVariables>) {
        return ApolloReactHooks.useMutation<DeleteNoteMutation, DeleteNoteMutationVariables>(DeleteNoteDocument, baseOptions);
      }
export type DeleteNoteMutationHookResult = ReturnType<typeof useDeleteNoteMutation>;
export type DeleteNoteMutationResult = ApolloReactCommon.MutationResult<DeleteNoteMutation>;
export type DeleteNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<DeleteNoteMutation, DeleteNoteMutationVariables>;