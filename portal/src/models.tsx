export interface NoteData {
  id: string
  title: string
  body: string
  xPos: number
  yPos: number
  sequence: number
  corkboardId: string
}
