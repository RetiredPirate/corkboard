import React, { useState, useCallback } from 'react'
import Draggable, { DraggableEvent, DraggableData } from 'react-draggable'
import styled from 'styled-components'
import { NoteData } from '../models'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faBars } from '@fortawesome/free-solid-svg-icons'
import { useUpdateNoteMutation, UpdateNote } from '../generated/graphql'

const NoteContainer = styled.div`
  background-color: yellow;
  border: 2px solid black;
  border-radius: 20px;
  height: 300px;
  width: 300px;
  display: flex;
  flex-direction: column;
  position: absolute;
`

const NoteTitleContainer = styled.div`
  border-bottom: 1px solid gray;
  display: flex;
`

const NoteTitleHandle = styled.strong`
  height: 100%;
  width: 50px;
  cursor: all-scroll;
  font-size: 2rem;
  padding-left: 10px;
`

const NoteTitleInput = styled.input`
  font-weight: bold;
  font-size: 200%;
  background-color: transparent;
  width: 100%;
  height: 100%;
  text-align: center;
  resize: none;
  border: none;
  outline: none;
  flex-grow: 1;
`

const NoteTitleDelete = styled.div`
  height: 100%;
  width: 50px;
  cursor: pointer;
  font-size: 2rem;
  padding-right: 10px;
`

const NoteBody = styled.div`
  flex-grow: 1;
`

const BodyTextArea = styled.textarea`
  background-color: transparent;
  width: 100%;
  height: 100%;
  text-align: center;
  resize: none;
  border: none;
  outline: none;
`

const Note: React.SFC<{
  note: NoteData
  pushToTop: () => void
  clickedDelete: () => void
}> = ({ note, pushToTop, clickedDelete }) => {
  const [titleText, setTitleText] = useState(note.title)
  const [bodyText, setBodyText] = useState(note.body)
  const [updateNote] = useUpdateNoteMutation()

  const updateTitleText = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const inputValue = e.target.value
      if (inputValue.length <= 10) {
        setTitleText(inputValue)
      }
    },
    []
  )

  const updateBodyText = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
      const textAreaValue = e.target.value
      if (textAreaValue.split(/\r*\n/).length <= 10) {
        setBodyText(textAreaValue)
      }
    },
    []
  )

  const saveTextChanges = useCallback((): void => {
    const noteToUpdate: UpdateNote = {
      id: note.id,
      title: titleText,
      body: bodyText
    }
    updateNote({ variables: { input: noteToUpdate } })
  }, [bodyText, note.id, titleText, updateNote])

  const savePositionChanges = useCallback(
    (_: DraggableEvent, data: DraggableData): void => {
      const noteToUpdate: UpdateNote = {
        id: note.id,
        xPos: data.lastX,
        yPos: data.lastY
      }
      updateNote({ variables: { input: noteToUpdate } })
    },
    [note.id, updateNote]
  )

  const saveOnClick = useCallback((): void => {
    const noteToUpdate: UpdateNote = {
      id: note.id
    }
    updateNote({ variables: { input: noteToUpdate } })

    pushToTop()
  }, [note.id, pushToTop, updateNote])

  return (
    <Draggable
      handle="strong"
      bounds="parent"
      defaultPosition={{ x: note.xPos, y: note.yPos }}
      onStart={() => pushToTop()}
      onStop={savePositionChanges}
      enableUserSelectHack={false}
    >
      <NoteContainer>
        <NoteTitleContainer>
          <NoteTitleHandle onMouseDown={saveOnClick}>
            <FontAwesomeIcon icon={faBars} />
          </NoteTitleHandle>
          <NoteTitleInput
            value={titleText}
            onChange={updateTitleText}
            onBlur={saveTextChanges}
            onClick={saveOnClick}
          ></NoteTitleInput>
          <NoteTitleDelete onClick={clickedDelete}>
            <FontAwesomeIcon icon={faTimes} />
          </NoteTitleDelete>
        </NoteTitleContainer>
        <NoteBody onClick={saveOnClick}>
          <BodyTextArea
            value={bodyText}
            onChange={updateBodyText}
            onBlur={saveTextChanges}
          ></BodyTextArea>
        </NoteBody>
      </NoteContainer>
    </Draggable>
  )
}

export default Note
