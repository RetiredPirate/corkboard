import React, { useContext } from 'react'
import { Link } from '@reach/router'
import ThemeContext, { IThemeModel } from '../ThemeContext/ThemeContext'
import styled from 'styled-components'

const MenuContainer = styled.div`
  display: flex;
  height: 4rem;
  justify-content: space-between;
  position: fixed;
  width: 100%;
  background-color: ${(props: { backgroundColor: string }) =>
    props.backgroundColor};
`

const MenuLogoWrapper = styled.div`
  cursor: pointer;
  padding: 0 0.5em 0 0.5em;
  margin: 0.5em;
  border-radius: 1em;
  transition: background-color 0.25s;

  :hover {
    background-color: ${(props: { backgroundColor: string }) =>
      props.backgroundColor};
  }
`

const MenuLogo = styled(({ fontColor, ...rest }) => <Link {...rest} />)`
  align-items: center;
  display: flex;
  font-size: 2.2rem;
  height: 100%;
  text-decoration: inherit;
  cursor: inherit;
  color: ${(props: { fontColor: string }) => props.fontColor};
  background-color: transparent;

  :hover {
    color: ${(props: { fontColor: string }) => props.fontColor};
    text-decoration: none currentcolor solid;
  }
`

const MenuBar: React.FC = () => {
  const [theme]: [IThemeModel, Function] = useContext(ThemeContext)

  return (
    <MenuContainer backgroundColor={theme.color2}>
      <MenuLogoWrapper backgroundColor={theme.color4}>
        <MenuLogo to="/" fontColor={theme.color3}>
          Corkboard
        </MenuLogo>
      </MenuLogoWrapper>
    </MenuContainer>
  )
}

export default MenuBar
