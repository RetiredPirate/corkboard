import React, { useState, useEffect, useCallback } from 'react'
import Note from './Note'
import { NoteData } from '../models'
import styled from 'styled-components'
import { Button } from 'reactstrap'
import {
  useCreateNoteMutation,
  useDeleteNoteMutation,
  useNotesQuery
} from '../generated/graphql'

const CorkboardContainer = styled.div`
  height: 92vh;
  position: relative;
`

const AddNoteButton = styled(Button)`
  position: fixed;
  bottom: 20px;
`

const Corkboard: React.FC = () => {
  const [notes, setNotes]: [Array<NoteData>, Function] = useState([])
  const { data: retrievedNotes } = useNotesQuery()
  const [createNote, { data: createdNote }] = useCreateNoteMutation()
  const [deleteNote] = useDeleteNoteMutation()

  const pushNoteToTop = useCallback((note: NoteData): void => {
    setNotes((currentNotes: Array<NoteData>) => {
      const noteArray = currentNotes.filter(n => n !== note)
      noteArray.push(note)
      return noteArray
    })
  }, [])

  useEffect(() => {
    if (retrievedNotes) {
      const sortedNotes = retrievedNotes.notes.sort((n1, n2) =>
        n1.sequence < n2.sequence ? -1 : 1
      )
      setNotes(sortedNotes)
    }
  }, [retrievedNotes])

  useEffect(() => {
    if (createdNote) {
      const newNote: NoteData = {
        id: createdNote.createNote.id,
        title: '',
        body: '',
        xPos: createdNote.createNote.xPos,
        yPos: createdNote.createNote.yPos,
        sequence: createdNote.createNote.sequence,
        corkboardId: '5e658f7743e73214e0ac8c44'
      }
      pushNoteToTop(newNote)
    }
  }, [createdNote, pushNoteToTop])

  const removeAndDeleteNote = useCallback(
    (idToDelete: string): void => {
      setNotes((notes: Array<NoteData>) =>
        notes.filter(n => n.id !== idToDelete)
      )
      deleteNote({ variables: { id: idToDelete } })
    },
    [deleteNote]
  )

  return (
    <CorkboardContainer>
      {notes.map((note: NoteData) => (
        <Note
          key={note.id}
          note={note}
          pushToTop={() => pushNoteToTop(note)}
          clickedDelete={() => removeAndDeleteNote(note.id)}
        />
      ))}
      <AddNoteButton
        color="success"
        onClick={() =>
          createNote({
            variables: { input: { corkboardId: '5e658f7743e73214e0ac8c44' } }
          })
        }
      >
        Add Note
      </AddNoteButton>
    </CorkboardContainer>
  )
}

export default Corkboard
