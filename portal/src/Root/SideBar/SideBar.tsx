import React, { useContext } from 'react'
import './SideBar.scss'
import { CSSTransition } from 'react-transition-group'
import { Button } from 'reactstrap'
import ThemeContext, {
  ThemeOptions,
  IThemeModel
} from '../../ThemeContext/ThemeContext'
import styled from 'styled-components'

const width = '300px'

const SidebarContainer = styled.div`
  width: ${width};
  position: fixed;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: ${(props: { background: string }) => props.background};
`

const SettingsTitle = styled.h2`
  padding: 0.35em 0;
  font-weight: lighter;
  color: ${(props: { color: string }) => props.color};
`

interface SideBarProps {
  isOpen: boolean
}

const SideBar: React.SFC<SideBarProps> = ({ isOpen }) => {
  const [theme, setTheme]: [IThemeModel, Function] = useContext(ThemeContext)

  return (
    <CSSTransition in={isOpen} timeout={500} classNames="sidebar" unmountOnExit>
      <SidebarContainer background={theme.color4}>
        <SettingsTitle color={theme.color3}>Settings</SettingsTitle>
        <Button onClick={() => setTheme(ThemeOptions.Dark)}>
          Dark Mode
        </Button>{' '}
        <Button onClick={() => setTheme(ThemeOptions.Light)}>Light Mode</Button>
      </SidebarContainer>
    </CSSTransition>
  )
}

export default SideBar
