import React, { useState, useContext } from 'react'
import MenuBar from './MenuBar'
import Corkboard from './Corkboard'
import SideBar from '../Root/SideBar/SideBar'
import { Button } from 'reactstrap'
import ThemeContext, { IThemeModel } from '../ThemeContext/ThemeContext'
import styled from 'styled-components'

const Container = styled.div`
  text-align: center;
  background-color: ${(props: { backgroundColor: string }) =>
    props.backgroundColor};
`

const RoutedComponent = styled.div`
  min-height: 100vh;
  padding-top: 4em;
`

const ToggleSidebarButton = styled(Button)`
  position: fixed;
  left: 10px;
  top: 5rem;
`

const Root: React.FC = () => {
  const [sidebarOpen, setSidebarOpen] = useState(false)
  const [theme]: [IThemeModel, Function] = useContext(ThemeContext)

  return (
    <Container backgroundColor={theme.color1}>
      <MenuBar></MenuBar>
      <RoutedComponent>
        <Corkboard />
      </RoutedComponent>
      <ToggleSidebarButton
        className="expand-sidebar-button"
        onClick={() => setSidebarOpen(!sidebarOpen)}
      >
        Settings
      </ToggleSidebarButton>
      <SideBar isOpen={sidebarOpen}></SideBar>
    </Container>
  )
}

export default Root
