import { useState } from "react";
import { ThemeOptions, IThemeModel } from "./ThemeContext";

const lightTheme: IThemeModel = {
  color1: "#c8ae92",
  color2: "#decbac",
  color3: "brown",
  color4: "#c8b499",
  color5: "purple"
};

const darkTheme: IThemeModel = {
  color1: "#2e2d32",
  color2: "#3e4144",
  color3: "#F7EDF0",
  color4: "#2e3136",
  color5: "#B6D6CC"
};

const useTheme = (): [IThemeModel, Function] => {
  const [theme, setTheme] = useState(darkTheme);

  const userSetTheme = (option: ThemeOptions) => {
    switch (option) {
      case ThemeOptions.Light:
        setTheme(lightTheme);
        break;
      default:
        setTheme(darkTheme);
    }
  };

  return [theme, userSetTheme];
};

export default useTheme;
