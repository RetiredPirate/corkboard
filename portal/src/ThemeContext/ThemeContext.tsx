import { createContext } from "react";

export interface IThemeModel {
  color1: string;
  color2: string;
  color3: string;
  color4: string;
  color5: string;
}

const defaultTheme: IThemeModel = {
  color1: "",
  color2: "",
  color3: "",
  color4: "",
  color5: ""
};

export enum ThemeOptions {
  Light,
  Dark
}

const deef: [IThemeModel, Function] = [defaultTheme, () => {}];

const ThemeContext = createContext(deef);

export default ThemeContext;
